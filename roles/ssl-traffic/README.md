SSL TRAFFIC MANAGEMENT DESIGN
=============================

Overview
========
The SSL handshake between TMP and KIE server happens using CA certs. 
Internal CA certificate was recommended for internal communication due to security issues. But the decision was taken from Cubic to use the External CA certificate.


KIE SERVER <> BUSINESS CENTRAL SSL TRAFFIC
===========================================
Traffic is encrypted/decrypted using self-signed-certificates and public keys.

Each of the server has a copy of the counterpart's self-signed public key.

The key is safetly stored in a keystore.


Flow of request and certificate validations between Client(through TMP Sytem) and KIE system
=============================================================================================
1.TMP system will call the KIE server through the firewall[kie.transporttest.com.au]
2.The firewall does the port forwarding from 443[Firewall-HTTPS] to 8443[KIE-HTTPS] port of KIE server. The URL context is retained
3.The RHDM will share the “kie.transporttest.com.au” certificate to the Client[In our case it is TMP system]
4.TMP system will validate the certificate with CA server. The CA certificate is signed by Godaddy CA servers. 
5.Once the CA certificate is validated, the TMP system approves the SSL handshake and then the data transfer will take place. -->

Flow of code logic and files involved
======================================
Step 1 (main.yml)                    : Set Variables to identify the server being configured and the respective directory.
Step 2 (keystore.yml)                : Create a keystore. Create a public-keys hosting directory >> Copy the default java keystore, /etc/pki/ca-trust/extracted/java/cacerts ,into the directory.
Step 3 (import_self_signed.yml)      : Copy into server the counterpart's self signed key into the common directory created in step 1.
                                      - If configuring DC,  get a copy of KIE self signed key,.der file, then import key into keystore
                                      - If configuring KIE, get a copy of DC self signed key,.der file, then import key into keystore
Step 4: (import_external_signed.yml) : Copy the CA signed public key file, .pfx, for transport.net.au found in /tmp into the common directory created.Import the cert into the keystore too.
                                         - As per the design document, only KIE communicates with TMP system , using the External signed CA certicates.
Step 5: (standalone_full_xml.yml)    : Edit the server's standalone-full.xml the https details so as to know where to get the keys from. Then restart the service.