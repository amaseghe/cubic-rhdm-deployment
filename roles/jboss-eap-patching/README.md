Overview
=========
The code installs JBOSS EAP using ZIP/Installer approach.

For that reason, the patching approach will be using a ZIP/Installer approach.

Important URLs
===============
https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/7.3/html-single/patching_and_upgrading_guide/index

https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/7.3/html-single/configuration_guide/index#management_cli_overview


Important Notes on ZIP/Installer Installation Patching
=======================================================
* If you apply a patch that updates a module, the new patched JARs that are used at runtime are stored in EAP_HOME/modules/system/layers/base/.overlays/PATCH_ID/MODULE. 
The original unpatched files are left in EAP_HOME/modules/system/layers/base/MODULE, but these JARs are not used at runtime.
In order to significantly decrease the size of cumulative patch releases for JBoss EAP 7, you now cannot perform a partial roll back of a cumulative patch. 
For a patch that has been applied, you will only be able to roll back the whole patch.

* For example, if you apply CP03 to JBoss EAP 7.0.0, you will not be able to roll back to CP01 or CP02. 
 If you would like the ability to roll back to each cumulative patch release, each cumulative patch must be applied separately in the order they were released.

Applying a Patch to JBoss EAP Using the Management CLI
=======================================================
1) Log in to the Red Hat Customer Portal, and download the patch file from JBoss EAP Software Downloads.

2) From the management CLI, apply the patch using the following command, including the appropriate path to the patch file:

patch apply /path/to/downloaded-patch.zip

===> You can do it as an embedded server (I.e runtime ontop of the host).
{{ JBOSS_HOME }}/bin/jboss-cli.sh --commands='embed-server --server-config=standalone-full.xml | patch apply {{ artifact_location }}/{{ jboss_eap_patch_zip_file }}'

3) Restart the JBoss EAP server for the patch to take effect:
shutdown --restart=true