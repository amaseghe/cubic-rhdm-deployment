
Cubic RHDM Deplyment Automation
=============================

The objective of the playbooks is to meet the automation requirements for the Red Hat Decision Manager Deployment Automation project

The playbooks are designed to meet the following 3 automation scopes:

1) Deploying of Decision Controller using Ansible.
2) Deploying of Kie Server deployment using Ansible.
3) Patching Decision Contoller using Ansible.
4) Patching Kie Server using Ansible.

Code Design
================
Code has been built using Ansible Roles which provide the following advantages :
- Cleaner code.
- Re-usable code.
- Easy to troubleshoot.

Ansible Roles and Functions
===========================

* jboss-eap-install: Contains tasks that deploy jboss-eap on the servers. Role has dependency and can be run on it's own.
* rhdm-central : Contains tasks that are specific to the Decision Central deployment. Role has a dependency on jboss-eap-install.
* rhdm-kie : Contains tasks that are specific to the Kie Server deployment. Role has a dependency on jboss-eap-install.
* jboss-eap-security : Contains tasks that are for security enhancements. These include:
   - a) generation of self-signed certificates 
   - b) jboss-eap vault 
* jboss-rhel-service : Contains taks that are for configuring and enabling jboss-eap to run as a RHEL service. Role has no dependencies.
* jboss-eap-patching : Contains tasks that automate patching of Jboss eap platform on both KIE and DC servers.

Playbooks
================
* app-summary.yml : A file called at the end of each playbook. It prints the summary details for the user.
* site.yml : Main playbook that deploys BOTH KIE Server and DC Server.
* patch-jboss-eap.yml : Main playbook for patching BOTH the KIE and DC Servers Jboss EAP versions.
* unittest-decision-central-installation.yml : Playbook that builds ONLY the Decision Central component of the RHDM.
* unittest-kie-server-installation.yml : Playbook that build ONLY the Kie Server component of the RHDM.
* unittest-patching-decision-central.yml : Playbook that  performs patching ONLY on the Decision Central.
* unittest-patching-kie-server.yml : Playbook that performs patching ONLY on the Kie Server.

Vars Files
==========
* common-vars.yml  : Contains variables that can be edited by the user depending on the environment.
* secrets-vars.yml : Contains variables that will be moved to the Hashicorp vault.

ansible.cfg
===========
Ansible config file that sets the environmental variables during run-time.

inventories
===========
Directory with inventory files within it.
Depending on the environment being run, one can use any of the three files.

logs
====
Directory containing logs collected during the ansbile runs.
Within the logs:

retries : Directory containing all the attempted runs and fails.
ansible.log : File that contains logs collected during the Ansible runs

issues.md
=========
File that contains all the running issues during the code development.
Contains the current situation of the code development.


Requirements to run the playbooks
==================================

* Ansible 2.8 and above on the controlling machine.
* The remote device must support SSH
* User account with sudo rights.
* RHEL 8.* on the remote hosts.
* Network access to gitlab for the DC Server.
* Network access to artifacts storage for both KIE & DC Servers.

How to run
===========

* Code is designed to be run as ansible core.
* site.yml : $ ansible-playbook  site.yml -i inventories/eit --ask-pass --ask-become-pass
* patch-jboss-eap.yml : $ ansible-playbook patch-jboss-eap.yml -i inventories/eit --ask-pass --ask-become-pass
* unittest-decision-central-installation.yml : $ ansible-playbook unittest-decision-central-installation.yml-i inventories/eit  --ask-pass --ask-become-pass
* unittest-kie-server-installation.yml : $ ansible-playbook unittest-kie-server-installation.yml -i inventories/eit --ask-pass --ask-become-pass
* unittest-patching-decision-central.yml : $ ansible-playbook unittest-patching-decision-central.yml -i inventories/eit --ask-pass --ask-become-pass
* unittest-patching-kie-server.yml : $ ansible-playbook unittest-patching-kie-server.yml -i inventories/eit --ask-pass --ask-become-pass

Author
=======
Name: Allan Maseghe
Email: amaseghe@redhat.com

