22nd Feb 2022
============

> Decision central code running end to end.
> Decision Central server has to be started with a manual command. Service set-up to be relooked.
> Kie server installation role not fully tested.
> Generation of certifications under roles/jboss-eap-security tested and working.
> Configuration of EAP vault under roles/jboss-eap-security tested and working.
> Storing of keys in keystore under roles/ssl-traffic tested and working.
> Patching code is completed and tested.